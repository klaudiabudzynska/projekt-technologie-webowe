const toggleMenu = () => {
    const TOGGLE_CLASS = 'active';
    const menu = document.getElementById('menu');
    const menuContainer = document.getElementById('menu-container');

    menu.classList.toggle(TOGGLE_CLASS);
    menuContainer.classList.toggle(TOGGLE_CLASS);
}