const stickyMenu = () => {
    const introSection = document.getElementById('o-nas');
    const menu = document.getElementById('menu-container');
    const introHeight = introSection.offsetHeight;

    if (isMobile()) {
        if (window.pageYOffset > 0) {
            menu.classList.add('sticky-mobile');
        } else {
            menu.classList.remove('sticky-mobile');
        }
        return;
    } else {
        menu.classList.remove('sticky-mobile');
    }

    if (window.pageYOffset > introHeight / 2) {
        menu.classList.add('sticky');
    } else {
        menu.classList.remove('sticky');
    }
}