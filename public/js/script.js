(function () {
    window.addEventListener('load', () => {
        const toggleMenuButton = document.getElementById('toggle');

        toggleMenuButton.addEventListener('click', () => toggleMenu());

        window.addEventListener('scroll', () => {
            parallax('earth', -0.1, -0.05);
            parallax('mars', -0.05);
            parallax('intro__title', 0.1, -0.05);

            stickyMenu();
        });

        intersectionObserver();
    })
})()
