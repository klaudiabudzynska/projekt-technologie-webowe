const intersectionObserver = () => {
    const offerItemElements = document.querySelectorAll('.offer__element');
    const reservationDescriptionElement = document.querySelector('.reservation__description');

    const observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            if (entry.intersectionRatio > 0) {
                entry.target.style.animation = 'show-ui-element 1s forwards';
            } else {
                entry.target.style.animation = 'none';
            }
        })

    })

    for (let i = 0; i < offerItemElements.length; ++i) {
        observer.observe(offerItemElements[i]);
    }

    observer.observe(reservationDescriptionElement);
}