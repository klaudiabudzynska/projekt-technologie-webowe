const parallax = (nodeClass, x = 0, y = 0, z = 0) => {
    const node = document.querySelector(`.${nodeClass}`);
    const scrollY = window.pageYOffset;

    const X = scrollY * x
    const Y = scrollY * y
    const Z = scrollY * z

    node.style.transform = `translate3d(${X}px, ${Y}px, ${Z}px)`;
}